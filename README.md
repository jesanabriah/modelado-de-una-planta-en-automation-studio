# Modelado de una planta en Automation Studio

Universidad Distrital Francisco José de Caldas

Ingeniería electrónica

Curso de electrónica industrial

Profesor: Humberto Gutiérrez

Diseño por: Jorge Eliécer Sanabria Hernández

Bogotá, Colombia – Octubre de 2019

Ver vídeo en YouTube en https://youtu.be/6LygqzMXh80

Adicionalmente al video visible en YouTube, se ha cambiado el piloto PS2 para que se active solamente despues de dos ciclos de molienda. Para visualizar mejor el comportamiento de la planta.
